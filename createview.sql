CREATE VIEW StudentAdvise AS
SELECT s.StudentID,s.StudentName,s.StudentSurname,d.DeptName,a.AdvisorName,a.AdvisorSurname
FROM Student AS s
LEFT JOIN Department AS d
ON s.DeptID = d.DeptID
LEFT JOIN Advisor AS a
ON s.AdvisorID = a.AdvisorID


